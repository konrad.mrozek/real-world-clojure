(ns real-world-app.http
  (:require [org.httpkit.server :as server]
            [clojure.string :as s]
            [real-world-app.db :as db]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [response]]))

(defn handler [req]
  (condp s/starts-with? (:uri req)  
    "/" (response {:ok true})
    "/items" (response (db/read-items))))

(defonce server
    (server/run-server (wrap-json-response #'handler) {:port 8080}))

