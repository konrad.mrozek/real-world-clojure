(ns real-world-app.model)

(defn owner [name address]
  {:owner/name name
   :owner/address address})

(defn make-basket [owner]
  {:basket/owner owner
   :basket/items []})

(defn make-item [name price]
  {:basket-item/name name
   :basket-item/price price})

(defn add-item [basket item]
  (update basket :basket/items conj item))

(defn count-basket-price [basket]
  (->> basket
       :basket/items
       (map :basket-item/price)
       (reduce + 0)))

(defn increased-price [item]
  (+ 1 (:basket-item/price item)))

#_(count-basket-price
    (-> (owner "Zenon" "Sesame Street")
        (make-basket)
        (add-item (make-item "Bread" 4))
        (add-item (make-item "USB" 10))))