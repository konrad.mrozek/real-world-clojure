(ns real-world-app.db
  (:require [clojure.java.jdbc :as jdbc]))

(def pg-uri
  {:connection-uri "jdbc:postgresql://localhost:5432/postgres?user=postgres&password=simple"})

(defn store-item! [item]
  (jdbc/insert! pg-uri :item
                {:name (:basket-item/name item)
                 :price (:basket-item/price item)}))

(defn read-items []
  (jdbc/query pg-uri
    ["select * from item"]
    {:row-fn #(do {:basket-item/name (:name %)
                   :basket-item/price (:price %)})}))
 
(defn store-owner! [owner]
  (jdbc/insert! pg-uri :owner
                {:name (:owner/name owner)
                 :address (:owner/address owner)}))

(defn read-items []
  (jdbc/query pg-uri
    ["select * from item"]
    {:row-fn #(do {:basket-item/name (:name %)
                   :basket-item/price (:price %)})}))
