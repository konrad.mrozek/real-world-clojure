create table owner (
    id serial primary key,
    name text not null,
    address text not null
);

create table basket (
    id serial primary key,
    created TIMESTAMP DEFAULT NOW()
);

create table item (
    id serial primary key,
    name text not null,
    price decimal
);

create table basket_items (
    basket_id int references basket(id),
    item_id int references item(id)
);