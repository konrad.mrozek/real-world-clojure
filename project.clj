(defproject real-world-app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.6"]
                 
                 [org.postgresql/postgresql "42.2.2.jre7"]
                 
                 [http-kit "2.2.0"]
                 [ring/ring-json "0.4.0"]]
  :main ^:skip-aot real-world-app.core
  :target-path "target/%s"
  :plugins [[lein-kibit "0.1.6"]
            [jonase/eastwood "0.2.6"]]
  :profiles {:uberjar {:aot :all}
             :repl {:plugins [[cider/cider-nrepl "0.16.0"]]
                    :dependencies [[org.clojure/tools.nrepl "0.2.12"]]}})
